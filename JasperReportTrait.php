<?php
namespace ticmakers\jasper;

require_once __DIR__ . '/src/JasperPHP/JasperPHP.php';

use JasperPHP\JasperPHP;

/**
 * Trait para implementar todas las funcionalidades de jasper
 * 
 * @package ticmakers
 * @subpackage jaster
 * @category Traits
 * 
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
trait JasperReportTrait
{
    /**
     * Ruta base donde están almacenados los binarios de los reportes
     * 
     * @var string
     */
    public $resourcePath;

    /**
     * URI donde se almacenará los archivos generados
     * 
     * @var string
     */
    public $tmpFolder = 'tmp/';

    /**
     * Parámetros que se pasarán al reporte
     * 
     * @var array
     */
    public $globalParams = [];

    /**
     * Configuración de conexión a la base de datos
     * 
     * @var array
     */
    public $dbConfig = [];

    /**
     * @var boolean
     */
    public $background = true;

    /**
     * @var boolean
     */
    public $redirect_output = false;

    /**
     * Indica si al imprimir llamar el método printFile se eliminará el archivo
     * 
     * @var boolean
     */
    public $deleteOnPrint = false;

    /**
     * Callback de complemento para el documento que se genera
     * 
     * @var string|function
     */
    public $outputSeed = null;

    /**
     * Nombre de el reporte que se generó
     * 
     * @var string
     */
    private $outputFilename = '';

    /**
     * Formato o formatos en el cual se generará el reporte y puede adoptar los valores:
     * pdf, rtf, xls, xlsx, docx, odt, ods, pptx, csv, html, xhtml, xml, jrprint
     * 
     * @var string|array
     */
    private $format = [];

    /**
     * Genera el reporte
     * 
     * @param string $report URI del reporte
     * @param string $filename Nombre para el archivo(s) generado
     * @param string|array $format Formato en el cual se quiere generar el reporte
     * @param array $params Parámetros del reporte
     * @return boolean|string
     */
    public function generate($report, $filename = null, $format = 'pdf', $params = [])
    {
        if (empty($filename)) {
            $filename = $this->getOutputFilename();
        }
        if (!is_array($format)) {
            $format = [$format];
        }
        $this->format = $format;
        $this->outputFilename = str_replace('/', '\\', trim($this->resourcePath, '/') .'/'. trim($this->tmpFolder, '/') .'/'. $filename);
        $reportPath = str_replace('/', '\\', trim($this->resourcePath, '/') .'/'. $report);
        $params = array_merge($params, $this->globalParams);
        try {
            $jasper = new JasperPHP($this->resourcePath);
            $jasper->process($reportPath, $this->outputFilename, $format, $params, $this->dbConfig, $this->background, $this->redirect_output)
                ->execute();
        } catch (\Exception $ex) {
            throw new \Exception('Se ha producido un error al generar el reporte');
        }
        return array_map(function($value) {
            $outputFilename = $this->outputFilename;
            $this->reset();
            return $outputFilename .'.'. $value;
        }, $this->format);
    }

    /**
     * Genera el nombre del archivo que se utilizará como salida
     * 
     * @return array
     */
    public function getOutputFilename()
    {
        $base = 'output';
        if (!empty($this->outputSeed)) {
            $base .= call_user_func($this->outputSeed);
        }
        return $base;
    }

    /**
     * Imprime en el reporte generado
     * 
     * @param string $filePath Path de el archivo generado
     */
    public function printFile($filePath)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
		header('Content-Type: ' . finfo_file($finfo, $filePath));
		finfo_close($finfo);
		header('Content-Disposition: attachment; filename='.basename($filePath));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($filePath));
		ob_clean();
		flush();
        readfile($filePath);
        if ($this->deleteOnPrint) {
            unlink($filePath);
        }
		exit;
    }

    /**
     * Reinicia las propiedades de la clase
     */
    public function reset()
    {
        $this->outputFilename = null;
        $this->outputSeed = null;
    }
}