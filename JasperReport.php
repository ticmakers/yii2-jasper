<?php

namespace ticmakers\jasper;

use Yii;
use ticmakers\jasper\JasperReportTrait;

/**
 * Implementación de JasperSoft.
 *
 * @package ticmakets
 * @subpackage jaspert
 * @category Component
 *
 * @author Kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class JasperReport extends \yii\base\BaseObject
{
    use JasperReportTrait;
}