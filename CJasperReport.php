<?php

require_once __DIR__ .'/JasperReportTrait.php';

use ticmakers\jasper\JasperReportTrait;

class CJasperReport extends CApplicationComponent
{
    use JasperReportTrait;

    /**
     * Método de inicialzación de componente
     */
    public function init()
    {
        parent::init();
        $this->resourcePath = Yii::getPathOfAlias($this->resourcePath);
    }
}